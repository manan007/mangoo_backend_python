from users.models import *
from datetime import datetime as dt, timedelta

def delete_status():
	yesterday = dt.today() - timedelta(days=1)
	y_date = yesterday.date()
	y_time = yesterday.time()

	status_list = UserStatus.objects.all()
	for status in status_list:
		uploaded_date_obj = status.created_at
		
		u_date = uploaded_date_obj.date()
		u_time = uploaded_date_obj.time()

		# print('u_date: ', u_date)
		# print('y_date: ', y_date)
		# print('u_time: ', u_time)
		# print('y_time: ', y_time)
		# print("------------------------------")

		if (u_date == y_date) and (u_time <= y_time):
			UserStatus.objects.get(pk = status.id).delete()
		