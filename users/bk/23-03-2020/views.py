from django.shortcuts import render
from django.utils.crypto import get_random_string
from random import randint

from users.serializers import *

from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from twilio.rest import Client

from django.conf import settings
from django.db.models import Q

import csv

import logging
import json

import os

from rest_framework.parsers import MultiPartParser

# Create your views here.

logger = logging.getLogger('info_logger')

twilio_client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN)


# -------------------------------------------------- OTP START -------------------------------------------------- #
class SendOTPView(APIView):
	logger.info('Send OTP')

	serializer_class = UserOTPSerializer
	def post(self, request):
		data = {}
		# try:
		phone_number = request.data.get('phone_number')

		if len(phone_number) < 10 or len(phone_number) > 18:
			logger.info('Phone NUmber length wrong')
			return Response({'status': False, 'response_msg': 'Please enter valid 10 digit mobile number'})
		else:
			verification_check = twilio_client.verify \
                           .services(settings.TWILIO_SERVICE_ID) \
                           .verifications \
                           .create(to = phone_number, channel = 'sms')
		
		data['verification_check_id'] = verification_check.sid
		data['response_msg'] = 'OTP Sent'

		logger.info('OTP Sent')
		return Response({ 'code': 200, 'status': 'success', 'data': data })
		# except:
		# 	logger.info('Error send otp')
		# 	data['response_msg'] = 'Something went wrong'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })


class VerifyOTPView(APIView):
	serializer_class = VerifyOTPSerializer
	def post(self, request):
		data = {}
		try:
			serializer = VerifyOTPSerializer(data = request.data)

			if serializer.is_valid():
				if ('device_id' in serializer.data) and ('device_type' in serializer.data):
					device_id = serializer.data['device_id']
					device_type = serializer.data['device_type']
					otp = request.POST.get('otp')
					phone_number = request.POST.get('phone_number')

					if len(otp) != 6:
						data['response_msg'] = 'Please enter 6 digit OTP'
						return Response({ 'code': 400, 'status': 'error', 'data': data })

					try:
						verification_check = twilio_client.verify \
										    .services(settings.TWILIO_SERVICE_ID) \
										    .verification_checks \
										    .create(to = phone_number, code = otp)
					except:
						data['response_msg'] = 'Invalid phone number or OTP'
						return Response({ 'code': 400, 'status': 'error', 'data': data })

					if verification_check.status == 'approved':
						token = get_random_string(length = 64)

						user_filter = Profile.objects.filter(phone_number = phone_number)
						if len(user_filter):
							user = user_filter.first()
							user.token = token
							user.device_type = device_type
							user.device_id = device_id
							user.save()
						else:
							user = Profile(phone_number = phone_number, token = token, device_type = device_type, device_id = device_id, is_verified = True)			
							user.save()

						data['token'] = token
						data['response_msg'] = 'OTP Verified'

						return Response({ 'code': 200, 'status': 'success', 'data': data})

					data['response_msg'] = 'Error in validation'
					return Response({ 'code': 400, 'status': 'error', 'data': data})
				else:
					phone_number = request.POST.get('phone_number')
					user_filter = Profile.objects.filter(phone_number = phone_number)
					if len(user_filter):
						user_data = Profile.objects.get(phone_number = phone_number)
						if user_data.is_verified:
							data['token'] = user_data.token
							data['response_msg'] = 'OTP Verified'
							code = 200
							status = 'success'
						else:
							data['response_msg'] = 'Please register first on the mobile device'
							code = 400
							status = 'error'
					else:
						code = 400
						status = 'error'
						data['response_msg'] = 'Please register first on the mobile device'
						
					return Response({ 'code': code, 'status': status, 'data': data })

		except:
			data['response_msg'] = 'Invalid Request'
			return Response({ 'code': 400, 'status': 'error', 'data': data})


class UserRegisterProfileAdd(APIView):
	serializer_class = UserRegisterProfileAddSerializer

	def post(self, request):
		data = {}
		try:
			logger.info('User Profile')
			serializer = UserRegisterProfileAddSerializer(data = request.data)

			if serializer.is_valid():
				name = request.POST['name']
				image = request.FILES['image']
				token = request.POST['token']

				if not token:
					logger.info('NO user token')
					data['response_msg'] = 'Please add user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				user_filter = Profile.objects.filter(token = token)
				if user_filter:
					user = Profile.objects.get(token = token)
					user.name = name
					user.image = image
					user.save()

					# SAVE IMAGE LOCATION IN ONE MODEL WITH USER'S PHONE NUMBER

					logger.info('Add profile success')
					data['response_msg'] = 'Successfully updated'
					# data['url'] = request.get_host() + '/media/' + str(user.image)

					return Response({ 'code': 200, 'status': 'success', 'data': data })

				logger.info('Wrong user token')
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			logger.info('Invalid data')
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			logger.info('Exception')
			data['response_msg'] = 'Invalid Request'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


# -------------------------------------------------- OTP STOP -------------------------------------------------- #

class getWhatsAppContacts(APIView):
	serializer_class = GetWhatsAppContactSerializer
	# parser_classes = (MultiPartParser, )

	def post(self, request, format=None):
		data = {}
		# try:
		serializer = GetWhatsAppContactSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			file = serializer.data['file']

			json_data = json.loads(file)

			whatsapp_contacts = []
			temp_whatsapp_contacts = []

			contact_string = ''
			check_user = Profile.objects.filter(token = token)
			if check_user:
				user = Profile.objects.get(token = token)
				user_phone_number = user.phone_number
				
				for contact_data in json_data:					
					phone_number = '+' + contact_data['contact']

					phone_exists = Profile.objects.filter(phone_number = phone_number)
					if phone_exists:
						temp = {}
						w_temp = {}
						whatsapp_contacts.append(phone_number)

						w_temp['phone_number'] = phone_number
						w_temp['name'] = contact_data['name']
						temp_whatsapp_contacts.append(w_temp)

				is_exists = Contact.objects.filter(user_phone_number = user_phone_number)
				if is_exists:
					user_data = Contact.objects.get(user_phone_number = user_phone_number)
					user_data.contacts = user_data.contacts + ','.join(whatsapp_contacts)
					# user_data.contacts = user_contacts
					user_data.save()
				else:
					save_contacts = Contact(user_phone_number = user_phone_number, contacts = ','.join(whatsapp_contacts))
					save_contacts.save()

				data['whatsapp_contacts'] = temp_whatsapp_contacts
				data['response_msg'] = "Whatsapp Contact List"
				return Response({ 'code': 200, 'status': 'success', 'data': data })


			data['response_msg'] = 'Invalid Token'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

			
		else:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		# except:
		# 	data['response_msg'] = 'Something went wrong'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })


class VideoChatsView(APIView):
	serializer_class = GetVideoCallsSerializer

	def post(self, request):
		data = {}
		try:
			token = request.data.get('token')

			check_user = Profile.objects.filter(token = token)
			if check_user:
				user = Profile.objects.get(token = token)

				video_chats = VideoChat.objects.filter(Q(sender = user.phone_number) | Q(receiver = user.phone_number))
				chats = []
				if video_chats:
					for chat in video_chats:
						chats.append(chat)

					data['chats'] = chats
					data['response_msg'] = 'Video Chats'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'No video chats to show'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			data['response_msg'] = 'Something went wrong'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class BlockedContactsView(APIView):
	serializer_class = GetBlockedContactsSerializer

	def post(self, request):
		try:
			data = {}
			serializer = GetBlockedContactsSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					phone_number = user_data.phone_number

					contacts_list = []
					filter_blocked_contacts = BlockedContact.objects.filter(blocker = phone_number)
					if filter_blocked_contacts:
						for i in filter_blocked_contacts:
							contacts_list.append(str(i.blocked_user))
						
						data['blocked_users'] = contacts_list
						return Response({ 'code': 200, 'status': 'success', 'data': data })

					data['response_msg'] = 'No blocked user found'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class AddBlockedContactView(APIView):
	serializer_class = AddBlockedContactsSerializer

	def post(self, request):
		try:
			data = {}
			serializer = AddBlockedContactsSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				phone_number = serializer.data['user_phone_number']

				filter_current_user = Profile.objects.filter(token = token)
				if filter_current_user:
					current_user_data = Profile.objects.get(token = token)
					current_phone_number = current_user_data.phone_number

					filter_block_user = Profile.objects.filter(phone_number = phone_number)
					if filter_block_user:
						block_user_data = Profile.objects.get(phone_number = phone_number)

						add_user_to_block_list = BlockedContact(blocker = current_phone_number, blocked_user = phone_number)
						add_user_to_block_list.save()

						data['response_msg'] = 'User blocked successfully'
						return Response({ 'code': 200, 'status': 'success', 'data': data })
						
					else:
						data['response_msg'] = 'Please enter valid phone number of a user you want to block'
						return Response({ 'code': 400, 'status': 'error', 'data': data })

				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


def get_image_link(request, image):
	return request.get_host() + settings.MEDIA_URL + image


class DisplayProfileView(APIView):
	serializer_class = DisplayProfileSerializer

	def post(self, request):
		try:			
			data = {}
			serializer = DisplayProfileSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					name = user_data.name
					status = user_data.status
					image = user_data.image

					data['name'] = name
					data['status'] = status
					data['image'] = request.get_host() + image.url
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserNameChangeView(APIView):
	serializer_class = EditNameSerializer

	def post(self, request):
		try:
			data = {}
			serializer = EditNameSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				name = serializer.data['name']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					user_data.name = name
					user_data.save()

					data['response_msg'] = 'Name changed successfully'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserStatusChangeView(APIView):
	serializer_class = EditStatusSerializer

	def post(self, request):
		try:
			data = {}
			serializer = EditStatusSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				status = serializer.data['status']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					user_data.status = status
					user_data.save()

					data['response_msg'] = 'Status changed successfully'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserProfilePhotoChangeView(APIView):
	serializer_class = EditImageSerializer

	def post(self, request):
		data = {}
		try:
			# logger.info('User Profile')
			serializer = EditImageSerializer(data = request.data)

			if serializer.is_valid():
				token = request.POST['token']
				image = request.FILES['image']
				# image = request.POST['image']

				if not token:
					logger.info('No user token')
					data['response_msg'] = 'Please add user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				if not image:
					data['response_msg'] = 'Please add user image'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				user_filter = Profile.objects.filter(token = token)
				if user_filter:
					user = Profile.objects.get(token = token)				
					user.image = image
					user.save()

					# SAVE IMAGE LOCATION IN ONE MODEL WITH USER'S PHONE NUMBER

					data['response_msg'] = 'Successfully updated'

					return Response({ 'code': 200, 'status': 'success', 'data': data })

				logger.info('Wrong user token')
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			logger.info('Invalid data')
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			logger.info('Exception')
			data['response_msg'] = 'Invalid Request'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


def privacy_policy(request):
	return render(request, template_name = 'privacy_policy.html')



def checkToken(token):
	user_filter = Profile.objects.filter(token = token)
	if user_filter:
		return True
	return False



# CHANGE IN ALL APIS TO RETURN IMAGE AND TEXT STATUS (text_status)

class AddStatusView(APIView):
	serializer_class = AddStatusSerializer

	def post(self, request):
		data = {}
		serializer = AddStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			token = serializer.data['token']
			
			text_status = ''
			if 'text_status' in serializer.data:
				text_status = serializer.data['text_status']

			file_type = ''
			user_status = ''
			if 'status' in request.FILES:
				upload = False
				user_status = request.FILES['status']
				extension = os.path.splitext(user_status.name)[1]

				if extension in ['.png', '.jpg', '.jpeg']:
					file_type = 'img'
					upload = True
				elif extension in ['.mp4', '.avi', '.gif', '.mkv']:
					file_type = 'video'
					upload = True

				if not upload:
					return Response({ 'code': 400, 'status': 'error', 'data': 'Please upload valid file. (.png, .jpg, .jpeg, .mp4, .avi, .mkv, .gif)' })

			card_color = ''
			if 'card_color' in serializer.data:
				card_color = serializer.data['card_color']

			if text_status or user_status:
				check_token = checkToken(token)
				if check_token:
					user = Profile.objects.get(token = token)
					phone_number = user.phone_number

					if user_status != '':
						add_user_status = UserStatus(user = phone_number, status = user_status, card_color = card_color, status_type = file_type)
					else:
						add_user_status = UserStatus(user = phone_number, text_status = text_status, card_color = card_color)

					add_user_status.save()

					data['response_msg'] = 'Successfully Added'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'Invalid User Token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

			data['response_msg'] = 'Upload 1 status'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DeleteStatusView(APIView):
	serializer_class = DeleteStatusSerializer

	def post(self, request):
		data = {}
		serializer = DeleteStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			status_id = serializer.data['status_id']

			check_token = checkToken(token)
			if check_token:
				find_status = UserStatus.objects.filter(pk = status_id)
				if find_status:
					find_status.delete()

					data['response_msg'] = 'Successfully Deleted'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'No status found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class ListMyContactStatusView(APIView):
	serializer_class = ListMyContactStatusSerializer

	def post(self, request):
		data = {}
		serializer = ListMyContactStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']

			check_token = checkToken(token)
			if check_token:
				status_list = []
				user = Profile.objects.get(token = token)
				phone_number = user.phone_number

				get_whatsapp_contacts = Contact.objects.filter(user_phone_number = phone_number)
				if get_whatsapp_contacts:
					get_contacts = Contact.objects.get(user_phone_number = phone_number)
					contacts_str = get_contacts.contacts
					contacts_list = contacts_str.split(',')

					for contact in contacts_list:
						contact_status_list = []
						check_blocking = BlockedContact.objects.filter(blocker = contact, blocked_user = phone_number)
						if not check_blocking:
							get_status = UserStatus.objects.filter(user = contact)
							if get_status:
								for status in get_status:
									tmp = {}
									tmp['status_id'] = status.id
									tmp['phone_number'] = contact
									tmp['card_color'] = str(status.card_color)
									tmp['created_at'] = status.created_at

									status_type = ''
									if status.status != '':
										tmp['status'] = str(status.status)
										tmp['status_type'] = 'file'
									else:
										tmp['status'] = str(status.text_status)
										tmp['status_type'] = 'text'

									tmp['is_seen'] = status.is_seen

									contact_status_list.append(tmp)
							status_list.append(contact_status_list)

					data['response_msg'] = 'Status List'
					data['status'] = status_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'No Whatsapp Contacts'
					return Response({ 'code': 200, 'status': 'success', 'data': data })


				data['response_msg'] = 'Successfully Added'
				return Response({ 'code': 200, 'status': 'success', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DisplayMyStatusView(APIView):
	serializer_class = DisplayMyStatusSerializer

	def post(self, request):
		data = {}
		serializer = DisplayMyStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']

			check_token = checkToken(token)
			if check_token:
				status_list = []
				user = Profile.objects.get(token = token)
				phone_number = user.phone_number

				filter_my_status = UserStatus.objects.filter(user = phone_number)
				if filter_my_status:
					for status in filter_my_status:
						temp = {}
						temp['status_id'] = status.id
						temp['card_color'] = status.card_color
						
						status_type = ''
						if status.status != '':
							temp['status'] = str(status.status)
							temp['status_type'] = 'file'
						else:
							temp['status'] = str(status.text_status)
							temp['status_type'] = 'text'
						
						temp['file_type'] = status.status_type
						temp['created_at'] = status.created_at
						status_list.append(temp)

					data['response_msg'] = 'My Status List'
					data['status'] = status_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'No Status'
					data['status'] = ''
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'Successfully Added'
				return Response({ 'code': 200, 'status': 'success', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DisplayContactStatusView(APIView):
	serializer_class = DeleteStatusSerializer

	def post(self, request):
		data = {}
		serializer = DeleteStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			status_id = serializer.data['status_id']

			check_token = checkToken(token)
			if check_token:
				find_status = UserStatus.objects.filter(pk = status_id)
				if find_status:
					status_data = UserStatus.objects.get(pk = status_id)
					status = []
					temp = {}
					temp['card_color'] = str(status_data.card_color)
					temp['created_at'] = status_data.created_at
					temp['file_type'] = status_data.status_type

					status_type = ''
					if status_data.status != '':
						temp['status'] = str(status_data.status)
						temp['status_type'] = 'file'
					else:
						temp['status'] = str(status_data.text_status)
						temp['status_type'] = 'text'

					status.append(temp)

					data['response_msg'] = 'Contact status'
					data['status'] = status

					status_data.is_seen = 1
					status_data.save()

					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'No status found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

















