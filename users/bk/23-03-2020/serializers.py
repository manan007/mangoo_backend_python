from users.models import *
from rest_framework import serializers


class UserOTPSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['phone_number']


class VerifyOTPSerializer(serializers.ModelSerializer):
	device_type = serializers.CharField(required = False)
	device_id = serializers.CharField(required = False)
	
	class Meta:
		model = Profile
		fields = ['phone_number', 'otp', 'device_type', 'device_id']


class UserRegisterProfileAddSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['name', 'image', 'token']


class GetWhatsAppContactSerializer(serializers.ModelSerializer):
	file = serializers.CharField(max_length = 1000)
	# file = serializers.FileField()

	
	class Meta:
		model = Profile
		fields = ['token', 'file']


class GetVideoCallsSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)

	class Meta:
		model = VideoChat
		fields = ['token']


class GetBlockedContactsSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)

	class Meta:
		model = BlockedContact
		fields = ['token']


class AddBlockedContactsSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	user_phone_number = serializers.CharField(max_length = 20)

	class Meta:
		model = BlockedContact
		fields = ['token', 'user_phone_number']



# LIST PROFILE IN WEBVIEW SETTINGS
class DisplayProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token']

# EDIT IMAGE IN WEBVIEW SETTINGS
class EditImageSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token', 'image']

# EDIT NAME IN WEBVIEW SETTINGS
class EditNameSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token', 'name']

# EDIT STATUS IN WEBVIEW SETTINGS
class EditStatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token', 'status']



# STATUS
class AddStatusSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	text_status = serializers.CharField(max_length = 200, required = False)
	card_color = serializers.CharField(max_length = 50, required = False)

	class Meta:
		model = UserStatus
		fields = ['token', 'status', 'text_status', 'card_color']

class DeleteStatusSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	status_id = serializers.CharField(max_length = 10)
	card_color = serializers.CharField(max_length = 50, required = False)

	class Meta:
		model = UserStatus
		fields = ['token', 'status_id', 'card_color']


class ListMyContactStatusSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)

class DisplayMyStatusSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)















