from users.models import *
from rest_framework import serializers
import base64
from django.core.files.base import ContentFile
import six
import uuid


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):        
        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')

            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            file_extension = self.get_file_extension(file_name, decoded_file)
            complete_file_name = "%s.%s" % (file_name, file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class UserOTPSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['phone_number']


class VerifyOTPSerializer(serializers.ModelSerializer):
	device_type = serializers.CharField(required = False)
	device_id = serializers.CharField(required = False)
	fcm_id = serializers.CharField(required = False)
	
	class Meta:
		model = Profile
		fields = ['phone_number', 'otp', 'device_type', 'device_id', 'fcm_id']


class UserRegisterProfileAddSerializer(serializers.ModelSerializer):
	image = Base64ImageField(max_length=None, use_url=True, required = False)
	
	class Meta:
		model = Profile
		fields = ['name', 'image', 'token']


# EDIT IMAGE IN WEBVIEW SETTINGS
class EditImageSerializer(serializers.ModelSerializer):
	image = Base64ImageField(max_length = None, use_url = True)
	
	class Meta:
		model = Profile
		fields = ['token', 'image']


class GetWhatsAppContactSerializer(serializers.ModelSerializer):
	file = serializers.CharField(max_length = 500000)
	country_code = serializers.CharField(max_length = 5, required = True)
	# file = serializers.FileField()

	
	class Meta:
		model = Profile
		fields = ['token', 'file', 'country_code']


class CallLogSaveSerializer(serializers.ModelSerializer):
	class Meta:
		model = CallLog
		fields = ['caller', 'receiver', 'time', 'call_date', 'screen_id']


class CallLogListSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)

	class Meta:
		model = CallLog
		fields = ['token']


class GetBlockedContactsSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)

	class Meta:
		model = BlockedContact
		fields = ['token']


class AddBlockedContactsSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	user_phone_number = serializers.CharField(max_length = 20)

	class Meta:
		model = BlockedContact
		fields = ['token', 'user_phone_number']



# LIST PROFILE IN WEBVIEW SETTINGS
class DisplayProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token']

# EDIT NAME IN WEBVIEW SETTINGS
class EditNameSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token', 'name']

# EDIT STATUS IN WEBVIEW SETTINGS
class EditStatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ['token', 'status']



# STATUS
class AddStatusSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	text_status = serializers.CharField(max_length = 200, required = False)
	card_color = serializers.CharField(max_length = 50, required = False)
	status = Base64ImageField(max_length = None, use_url = True, required = False)

	class Meta:
		model = UserStatus
		fields = ['token', 'status', 'text_status', 'card_color']


class DeleteStatusSerializer(serializers.ModelSerializer):
	token = serializers.CharField(max_length = 100)
	status_id = serializers.CharField(max_length = 10)
	card_color = serializers.CharField(max_length = 50, required = False)

	class Meta:
		model = UserStatus
		fields = ['token', 'status_id', 'card_color']


class ListMyContactStatusSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)

class DisplayMyStatusSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)

class ChannelNameSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)
	phone_number = serializers.CharField(max_length = 15)

class SendNotificationSerializer(serializers.ModelSerializer):
	screen_id = serializers.CharField(required = False)

	class Meta:
		model = Profile
		fields = ['token', 'phone_number', 'screen_id']



# ----------------------------------- V2 -----------------------------------


class DeleteDataSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)

class UserLocationSerializer(serializers.Serializer):
	token = serializers.CharField(max_length = 100)
	lat = serializers.CharField(max_length = 50)
	lng = serializers.CharField(max_length = 50)

class SendCustomNotificationSerializer(serializers.Serializer):
	phone_number = serializers.CharField(max_length = 20)
	msg = serializers.CharField(max_length = 250)





















