from django.shortcuts import render
from django.utils.crypto import get_random_string
from random import randint

from users.serializers import *

from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from twilio.rest import Client

from django.conf import settings
from django.db.models import Q

import csv


# Create your views here.
twilio_client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN)

class SendOTPView(APIView):
	serializer_class = UserOTPSerializer
	def post(self, request):
		try:
			phone_number = request.data.get('phone_number')

			if len(phone_number) < 10 or len(phone_number) > 18:
				return Response({'status': False, 'response_msg': 'Please enter valid 10 digit mobile number'})
			else:
				verification_check = twilio_client.verify \
	                           .services(settings.TWILIO_SERVICE_ID) \
	                           .verifications \
	                           .create(to = phone_number, channel = 'sms')

				return Response({'status': True, 'response_msg': 'OTP sent', 'data': verification_check.sid })
		except:
			return Response({'status': False, 'response_msg': 'Something went wrong' })



class VerifyOTPView(APIView):
	serializer_class = VerifyOTPSerializer
	def post(self, request):
		try:
			otp = request.POST.get('otp')
			phone_number = request.POST.get('phone_number')

			if len(otp) != 6:
				return Response({'status': False, 'response_msg': 'Invalid OTP'})

			verification_check = twilio_client.verify \
							    .services(settings.TWILIO_SERVICE_ID) \
							    .verification_checks \
							    .create(to = phone_number, code = otp)

			if verification_check.status == 'approved':
				token = get_random_string(length = 64)

				user_filter = Profile.objects.filter(phone_number = phone_number)
				if len(user_filter):
					user = user_filter.first()
					user.token = token
					user.save()
				else:
					user = Profile(phone_number = phone_number, token = token)			
					user.save()

				return Response({'status': True, 'response_msg': 'OTP verified', 'token': token})

			return Response({'status': False, 'response_msg': 'Error in validation'})
		except:
			return Response({'status': False, 'response_msg': 'Invalid Request'})



class UserRegisterProfileAdd(APIView):
	serializer_class = UserRegisterProfileAddSerializer

	def post(self, request):
		try:
			serializer = UserRegisterProfileAddSerializer(data = request.data)

			if serializer.is_valid():
				name = request.POST['name']
				image = request.POST['image']
				token = request.POST['token']

				user_filter = Profile.objects.filter(token = token)
				if user_filter:
					user = Profile.objects.get(token = token)
					user.name = name
					user.image = image
					user.save()

					return Response({'status': True, 'response_msg': 'Successfully verified' })

				return Response({'status': False, 'response_msg': 'Invalid User data' })

			return Response({'status': False, 'response_msg': 'Invalid Data' })
		except:
			return Response({'status': False, 'response_msg': 'Invalid request' })



class getWhatsAppContacts(APIView):
	serializer_class = GetWhatsAppContactSerializer

	def post(self, request, format=None):
		try:
			file = request.data.get('file')
			token = request.data.get('token')

			whatsapp_contacts = []
			check_user = Profile.objects.filter(token = token)
			if check_user:
				user = Profile.objects.get(token = token)
			
				try:
					with open(file) as f:
						readCSV = csv.reader(f, delimiter = ',')
						for row in readCSV:
							if row[0] != '':
								phone_number = '+' + row[0]

								phone_exists = Profile.objects.filter(phone_number = phone_number)
								if phone_exists:
									user_data = Profile.objects.get(phone_number = phone_number)
									temp = {}
									temp['phone_number'] = phone_number
									temp['name'] = user_data.name
									temp['image'] = user_data.image
									whatsapp_contacts.append(temp)

						return Response({'status': True, 'response_msg': whatsapp_contacts })
				except:
					return Response({'status': False, 'response_msg': "Can't open file" })

			return Response({'status': False, 'response_msg': 'Invalid token' })

		except:
			return Response({'status': False, 'response_msg': 'Something went wrong' })



class VideoChatsView(APIView):
	serializer_class = GetVideoCallsSerializer

	def post(self, request):
		try:
			token = request.data.get('token')

			check_user = Profile.objects.filter(token = token)
			if check_user:
				user = Profile.objects.get(token = token)

				video_chats = VideoChat.objects.filter(Q(sender = user.phone_number) | Q(receiver = user.phone_number))
				chats = []
				if video_chats:
					for chat in video_chats:
						chats.append(chat)
					return Response({'status': True, 'response_msg': chats })
				else:
					return Response({'status': True, 'response_msg': 'No video chats to show' })
		except:
			return Response({'status': False, 'response_msg': 'Something went wrong' })



class OneToOneChatView(APIView):
	pass