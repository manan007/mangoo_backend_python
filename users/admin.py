from django.contrib import admin
from users.models import *

# Register your models here.
class ProfileAdmin(admin.ModelAdmin):
	list_display = ['name', 'phone_number', 'token', 'created_at', 'updated_at']

admin.site.register(Profile, ProfileAdmin)

admin.site.register(CallLog)
admin.site.register(Contact)

class BlockContactAdmin(admin.ModelAdmin):
	list_display = ['blocker', 'blocked_user']
admin.site.register(BlockedContact, BlockContactAdmin)

class StatusAdmin(admin.ModelAdmin):
	list_display = ['user', 'created_at']
admin.site.register(UserStatus, StatusAdmin)