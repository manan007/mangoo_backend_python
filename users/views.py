import csv
import uuid
import logging
import json
import ast 
import os
import base64
import requests

from pyfcm import FCMNotification

from django.shortcuts import render, get_object_or_404
from django.utils.crypto import get_random_string
from django.conf import settings
from django.db.models import Q
from django.core.files.base import ContentFile

from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.parsers import MultiPartParser, JSONParser

from twilio.rest import Client
from twilio.http.http_client import TwilioHttpClient

from random import randint
from mimetypes import guess_extension
from users.serializers import *

from fcm.utils import get_device_model

# Create your views here.
Device = get_device_model()

logger = logging.getLogger('info_logger')

# proxy_client = TwilioHttpClient(proxy = {'http': os.environ['http_proxy'], 'https': os.environ['https_proxy']})

# twilio_client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN, http_client = proxy_client)

twilio_client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN)

push_service = FCMNotification(api_key = settings.FCM_APIKEY)



def checkToken(token):
	user_filter = Profile.objects.filter(token = token)
	if user_filter:
		return True
	return False


def base64_file(data, name=None):
	_format, _img_str = data.split(';base64,')
	_name, ext = _format.split('/')
	if not name:
		name = _name.split(":")[-1]
	return ContentFile(base64.b64decode(_img_str), name='{}.{}'.format(name, ext))

def privacy_policy(request):
	return render(request, template_name = 'privacy_policy.html')

def get_image_link(request, image):
	return request.get_host() + settings.MEDIA_URL + image



# -------------------------------------------------- OTP START -------------------------------------------------- #
class SendOTPView(APIView):
	logger.info('Send OTP')

	serializer_class = UserOTPSerializer
	def post(self, request):
		data = {}
		try:
			phone_number = request.data.get('phone_number')

			if len(phone_number) < 10 or len(phone_number) > 18:
				logger.info('Phone NUmber length wrong')
				return Response({'status': False, 'response_msg': 'Please Enter Valid Number'})
			else:
				verification_check = twilio_client.verify \
	                           .services(settings.TWILIO_SERVICE_ID) \
	                           .verifications \
	                           .create(to = phone_number, channel = 'sms')
			
			data['verification_check_id'] = verification_check.sid
			data['response_msg'] = 'OTP Sent'

			logger.info('OTP Sent')
			return Response({ 'code': 200, 'status': 'success', 'data': data })
		except:
			logger.info('Error send otp')
			data['response_msg'] = 'Please Enter Valid Number'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class VerifyOTPView(APIView):
	serializer_class = VerifyOTPSerializer
	def post(self, request):
		data = {}
		# try:
		serializer = VerifyOTPSerializer(data = request.data)
		if serializer.is_valid():
			if ('device_id' in serializer.data) and ('device_type' in serializer.data):
				device_id = serializer.data['device_id']
				device_type = serializer.data['device_type']
				fcm_id = serializer.data['fcm_id']
				otp = request.POST.get('otp')
				phone_number = request.POST.get('phone_number')

				if len(otp) != 6:
					data['response_msg'] = 'Please enter 6 digit OTP'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				try:
					verification_check = twilio_client.verify \
									    .services(settings.TWILIO_SERVICE_ID) \
									    .verification_checks \
									    .create(to = phone_number, code = otp)
				except:
					data['response_msg'] = 'Invalid phone number or OTP'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				if verification_check.status == 'approved':
					token = get_random_string(length = 50)

					user_filter = Profile.objects.filter(phone_number = phone_number)
					if len(user_filter):
						user = user_filter.first()
						user.token = token
						user.device_type = device_type
						user.device_id = device_id
						user.save()
					else:
						user = Profile(phone_number = phone_number, token = token, device_type = device_type, device_id = device_id, is_verified = True)
						user.save()

					if Device.objects.filter(reg_id = fcm_id).exists():
						device = Device.objects.get(reg_id = fcm_id)
						device.dev_id = token
						device.save()
					else:
						device = Device(dev_id = token, reg_id = fcm_id, is_active = True).save()

					data['token'] = token
					data['response_msg'] = 'OTP Verified'

					return Response({ 'code': 200, 'status': 'success', 'data': data})

				data['response_msg'] = 'Please Enter Valid OTP'
				return Response({ 'code': 400, 'status': 'error', 'data': data})
			else:
				phone_number = request.POST.get('phone_number')
				user_filter = Profile.objects.filter(phone_number = phone_number)
				if len(user_filter):
					user_data = Profile.objects.get(phone_number = phone_number)
					if user_data.is_verified:
						data['token'] = user_data.token
						data['response_msg'] = 'OTP Verified'
						code = 200
						status = 'success'
					else:
						data['response_msg'] = 'Please register first on the mobile device'
						code = 400
						status = 'error'
				else:
					code = 400
					status = 'error'
					data['response_msg'] = 'Please register first on the mobile device'
					
				return Response({ 'code': code, 'status': status, 'data': data })
		else:
			data['response_msg'] = 'Not valid data'
			return Response({ 'code': 400, 'status': 'error', 'data': data})
		# except:
		# 	data['response_msg'] = 'Invalid Request'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data})


class UserRegisterProfileAdd(APIView):
	serializer_class = UserRegisterProfileAddSerializer

	def post(self, request):
		data = {}
		# try:
		logger.info('User Profile')
		serializer = UserRegisterProfileAddSerializer(data = request.data)

		if serializer.is_valid():
			name = request.POST['name']
			image = request.POST.get('image', None)
			token = request.POST['token']

			if name == '':
				data['response_msg'] = 'Please Enter Name'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			if not token:
				logger.info('NO user token')
				data['response_msg'] = 'Please add user token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			user_filter = Profile.objects.filter(token = token)
			if user_filter:
				user = Profile.objects.get(token = token)
				user.name = name

				if image is None:
					user.image = ''
				else:
					user.image = base64_file(image)
				user.save()

				logger.info('Add profile success')
				data['response_msg'] = 'Successfully updated'
				try:
					data['profile_image'] = user.image.url
				except:
					data['profile_image'] = ''
				# data['url'] = request.get_host() + '/media/' + str(user.image)

				return Response({ 'code': 200, 'status': 'success', 'data': data })

			logger.info('Wrong user token')
			data['response_msg'] = 'Invalid User Token'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		logger.info('Invalid data')
		data['response_msg'] = 'Invalid Data'
		return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })
		# except:
		# 	logger.info('Exception')
		# 	data['response_msg'] = 'Invalid Request'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })
# -------------------------------------------------- OTP STOP -------------------------------------------------- #




class getWhatsAppContacts(APIView):
	serializer_class = GetWhatsAppContactSerializer
	# parser_classes = (MultiPartParser, )

	def post(self, request, format=None):
		data = {}
		try:
			serializer = GetWhatsAppContactSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				file = serializer.data['file']
				country_code = serializer.data['country_code']

				json_data = json.loads(file)

				whatsapp_contacts = []
				temp_whatsapp_contacts = []

				contact_string = ''
				check_user = Profile.objects.filter(token = token)
				if check_user:
					user = Profile.objects.get(token = token)
					user_phone_number = user.phone_number
					
					for contact_data in json_data:
						phone_number = contact_data['contact']
						if not phone_number.startswith('+'):
							phone_number = str(country_code) + contact_data['contact']

						phone_exists = Profile.objects.filter(phone_number = phone_number)
						if phone_exists:
							if not BlockedContact.objects.filter(blocker = phone_number, blocked_user = user_phone_number).exists():
								temp = {}
								w_temp = {}

								if phone_number not in whatsapp_contacts:
									whatsapp_contacts.append(phone_number)

								w_temp['phone_number'] = phone_number
								w_temp['name'] = contact_data['name']

								try:
									get_contact_data = Profile.objects.get(phone_number = phone_number)
									profile_image = get_contact_data.image
									w_temp['profile_image'] = profile_image.url
								except:
									w_temp['profile_image'] = ''

								if not any(contact['phone_number']==phone_number for contact in temp_whatsapp_contacts):
									temp_whatsapp_contacts.append(w_temp)

					Contact.objects.filter(user_phone_number = user_phone_number).delete()
					save_contacts = Contact(user_phone_number = user_phone_number, contacts = temp_whatsapp_contacts)
					save_contacts.save()

					data['whatsapp_contacts'] = temp_whatsapp_contacts
					data['response_msg'] = "Whatsapp Contact List"
					return Response({ 'code': 200, 'status': 'success', 'data': data })


				data['response_msg'] = 'Invalid Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
			else:
				data['response_msg'] = 'Invalid Parameters'
				return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })
		except:
			data['response_msg'] = 'Something went wrong'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class BlockedContactsView(APIView):
	serializer_class = GetBlockedContactsSerializer

	def post(self, request):
		try:
			data = {}
			serializer = GetBlockedContactsSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					phone_number = user_data.phone_number

					contacts_list = []
					filter_blocked_contacts = BlockedContact.objects.filter(blocker = phone_number)
					if filter_blocked_contacts:
						for i in filter_blocked_contacts:
							contacts_list.append(str(i.blocked_user))
						
						data['blocked_users'] = contacts_list
						return Response({ 'code': 200, 'status': 'success', 'data': data })

					data['response_msg'] = 'No blocked user found'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class AddBlockedContactView(APIView):
	serializer_class = AddBlockedContactsSerializer

	def post(self, request):
		try:
			data = {}
			serializer = AddBlockedContactsSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				phone_number = serializer.data['user_phone_number']

				filter_current_user = Profile.objects.filter(token = token)
				if filter_current_user:
					current_user_data = Profile.objects.get(token = token)
					current_phone_number = current_user_data.phone_number

					filter_block_user = Profile.objects.filter(phone_number = phone_number)
					if filter_block_user:
						block_user_data = Profile.objects.get(phone_number = phone_number)

						add_user_to_block_list = BlockedContact(blocker = current_phone_number, blocked_user = phone_number)
						add_user_to_block_list.save()

						data['response_msg'] = 'User blocked successfully'
						return Response({ 'code': 200, 'status': 'success', 'data': data })
						
					else:
						data['response_msg'] = 'Please enter valid phone number of a user you want to block'
						return Response({ 'code': 400, 'status': 'error', 'data': data })

				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DisplayProfileView(APIView):
	serializer_class = DisplayProfileSerializer

	def post(self, request):
		try:			
			data = {}
			serializer = DisplayProfileSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)

					try:
						data['image'] = user_data.image.url
					except:
						data['image'] = ''

					data['name'] = user_data.name
					data['status'] = user_data.status
					
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserNameChangeView(APIView):
	serializer_class = EditNameSerializer

	def post(self, request):
		try:
			data = {}
			serializer = EditNameSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				name = serializer.data['name']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					user_data.name = name
					user_data.save()

					data['response_msg'] = 'Name changed successfully'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserProfilePhotoChangeView(APIView):
	serializer_class = EditImageSerializer

	def post(self, request):
		data = {}
		try:
			# logger.info('User Profile')
			serializer = EditImageSerializer(data = request.data)

			if serializer.is_valid():
				token = request.POST['token']
				# image = request.FILES['image']
				image = request.POST['image']

				if not token:
					logger.info('No user token')
					data['response_msg'] = 'Please add user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				if not image:
					data['response_msg'] = 'Please add user image'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

				user_filter = Profile.objects.filter(token = token)
				if user_filter:
					user = Profile.objects.get(token = token)					
					user.image = base64_file(image)
					user.save()

					# SAVE IMAGE LOCATION IN ONE MODEL WITH USER'S PHONE NUMBER

					data['response_msg'] = 'Successfully updated'

					return Response({ 'code': 200, 'status': 'success', 'data': data })

				logger.info('Wrong user token')
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			logger.info('Invalid data')
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		except:
			logger.info('Exception')
			data['response_msg'] = 'Invalid Request'
			return Response({ 'code': 400, 'status': 'error', 'data': data })





# -------------------------------------------------- STATUS START -------------------------------------------------- #
class AddStatusView(APIView):
	serializer_class = AddStatusSerializer

	def post(self, request):
		data = {}
		serializer = AddStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			
			text_status = ''
			if 'text_status' in serializer.data:
				text_status = serializer.data['text_status']
				
				if text_status == '':
					data['response_msg'] = 'Text Cannot be blank'
					return Response({ 'code': 400, 'status': 'success', 'data': data })

			file_type = ''
			user_status = ''
			if 'status' in request.POST:		
				upload = False
				user_status = request.POST['status']
				format, imgstr = user_status.split(';base64,')
				extension = format.split('/')[-1]

				if extension in ['png', 'jpg', 'jpeg']:
					file_type = 'img'
					upload = True
				elif extension in ['mp4', 'avi', 'gif', 'mkv']:
					file_type = 'video'
					upload = True

				if not upload:
					return Response({ 'code': 400, 'status': 'error', 'data': 'Please upload valid file. (.png, .jpg, .jpeg, .mp4, .avi, .mkv, .gif)' })

			card_color = ''
			if 'card_color' in serializer.data:
				card_color = serializer.data['card_color']

			if text_status or user_status:
				check_token = checkToken(token)
				if check_token:
					user = Profile.objects.get(token = token)
					phone_number = user.phone_number

					if user_status != '':
						add_user_status = UserStatus(user = phone_number, status = base64_file(user_status), card_color = card_color, status_type = file_type)
					else:
						add_user_status = UserStatus(user = phone_number, text_status = text_status, card_color = card_color)

					add_user_status.save()

					data['response_msg'] = 'Successfully Added'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'Invalid User Token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })

			data['response_msg'] = 'Upload 1 status'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })


class DeleteStatusView(APIView):
	serializer_class = DeleteStatusSerializer

	def post(self, request):
		data = {}
		serializer = DeleteStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			status_id = serializer.data['status_id']

			check_token = checkToken(token)
			if check_token:
				find_status = UserStatus.objects.filter(pk = status_id)
				if find_status:
					find_status.delete()

					data['response_msg'] = 'Successfully Deleted'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'No status found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class UserStatusChangeView(APIView):
	serializer_class = EditStatusSerializer

	def post(self, request):
		try:
			data = {}
			serializer = EditStatusSerializer(data = request.data)

			if serializer.is_valid():
				token = serializer.data['token']
				status = serializer.data['status']

				filter_user = Profile.objects.filter(token = token)
				if filter_user:
					user_data = Profile.objects.get(token = token)
					user_data.status = status
					user_data.save()

					data['response_msg'] = 'Status changed successfully'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					data['response_msg'] = 'Invalid user token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
		except:
			data['response_msg'] = 'Invalid Parameters'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DisplayMyStatusView(APIView):
	serializer_class = DisplayMyStatusSerializer

	def post(self, request):
		data = {}
		serializer = DisplayMyStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']

			check_token = checkToken(token)
			if check_token:
				status_list = []
				user = Profile.objects.get(token = token)
				phone_number = user.phone_number

				status_dict = {}				
				temp_dict = {}
				try:
					temp_dict['profile_image'] = user.image.url
				except:
					temp_dict['profile_image'] = ''
				status_dict['user_details'] = temp_dict

				filter_my_status = UserStatus.objects.filter(user = phone_number)
				if filter_my_status:
					my_status_list = []
					for status in filter_my_status:
						temp = {}
						temp['status_id'] = status.id
						temp['card_color'] = status.card_color
						
						status_type = ''
						if status.status != '':
							temp['status'] = status.status.url
							temp['status_type'] = 'file'
						else:
							temp['status'] = str(status.text_status)
							temp['status_type'] = 'text'
						
						temp['file_type'] = status.status_type
						temp['created_at'] = status.created_at
						
						my_status_list.append(temp)

					status_dict['my_status'] = my_status_list

					status_list.append(status_dict)

					data['response_msg'] = 'My Status List'
					data['status'] = status_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })
				else:
					status_list.append(status_dict)
					data['response_msg'] = 'No Status'
					data['status'] = status_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })
			else:
				data['response_msg'] = 'Invalid token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class ListMyContactStatusView(APIView):
	serializer_class = ListMyContactStatusSerializer

	def post(self, request):
		data = {}
		serializer = ListMyContactStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']

			check_token = checkToken(token)
			if check_token:
				status_list = []
				user = Profile.objects.get(token = token)
				phone_number = user.phone_number

				get_whatsapp_contacts = Contact.objects.filter(user_phone_number = phone_number)
				if get_whatsapp_contacts:
					get_contacts = Contact.objects.get(user_phone_number = phone_number)
					contacts_str = get_contacts.contacts

					# try:
					contacts_list = ast.literal_eval(contacts_str)
					for contact in contacts_list:
						contact_status_list = {}
						temp_user_data = {}

						check_blocking = BlockedContact.objects.filter(blocker = contact['phone_number'], blocked_user = phone_number)
						if not check_blocking:
							get_status = UserStatus.objects.filter(user = contact['phone_number'])
							if get_status:
								contact_data = Profile.objects.get(phone_number = contact['phone_number'])
								try:
									contact['profile_image'] = contact_data.image.url
								except:
									contact['profile_image'] = ''
								contact_status_list['user_details'] = contact

								user_status_list = []
								for status in get_status:
									tmp = {}
									tmp['status_id'] = status.id
									tmp['card_color'] = str(status.card_color)
									tmp['created_at'] = status.created_at

									if status.status != '':
										tmp['status'] = status.status.url
										tmp['status_type'] = 'file'
									else:
										tmp['status'] = str(status.text_status)
										tmp['status_type'] = 'text'

									tmp['is_seen'] = status.is_seen
									user_status_list.append(tmp)

								contact_status_list['user_status'] = user_status_list
						
						if contact_status_list:
							status_list.append(contact_status_list)
					# except:
					# 	pass


					data['response_msg'] = 'Status List'
					data['status'] = status_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'No Whatsapp Contacts'
					return Response({ 'code': 200, 'status': 'success', 'data': data })


				data['response_msg'] = 'Successfully Added'
				return Response({ 'code': 200, 'status': 'success', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })


class DisplayContactStatusView(APIView):
	serializer_class = DeleteStatusSerializer

	def post(self, request):
		data = {}
		serializer = DeleteStatusSerializer(data = request.data)

		if serializer.is_valid():
			token = serializer.data['token']
			status_id = serializer.data['status_id']

			check_token = checkToken(token)
			if check_token:
				find_status = UserStatus.objects.filter(pk = status_id)
				if find_status:
					status_data = UserStatus.objects.get(pk = status_id)
					status = []
					temp = {}
					temp['card_color'] = str(status_data.card_color)
					temp['created_at'] = status_data.created_at
					temp['file_type'] = status_data.status_type

					status_type = ''
					if status_data.status != '':
						temp['status'] = str(status_data.status)
						temp['status_type'] = 'file'
					else:
						temp['status'] = str(status_data.text_status)
						temp['status_type'] = 'text'

					status.append(temp)

					data['response_msg'] = 'Contact status'
					data['status'] = status

					status_data.is_seen = 1
					status_data.save()

					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'No status found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			else:
				data['response_msg'] = 'Invalid User Token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			data['response_msg'] = 'Invalid Data'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

# -------------------------------------------------- STATUS STOP -------------------------------------------------- #




# ------------------------------------------- CHAT, NOTIFICATION START -------------------------------------------- #
class ChannelNameView(APIView):
	serializer_class = ChannelNameSerializer

	def post(self, request):
		try:
			serializer = ChannelNameSerializer(data = request.data)

			if serializer.is_valid():
				data = {}
				token = request.POST['token']
				phone_number = request.POST['phone_number']

				check_token = checkToken(token)
				if check_token:
					if Profile.objects.filter(phone_number = phone_number).exists():
						channel_name = uuid.uuid4()
						sender = Profile.objects.get(token = token)
						sender.is_host = True
						sender.channel_name = channel_name
						sender.save()

						receiver = Profile.objects.get(phone_number = phone_number)
						receiver.channel_name = channel_name
						receiver.save()

						data['channel_name'] = channel_name
						return Response({ 'code': 200, 'status': 'success', 'data': data })
					else:
						data['response_msg'] = 'Receiver not found'
						return Response({ 'code': 400, 'status': 'error', 'data': data })
				else:
					data['response_msg'] = 'Invalid token'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
			else:
				return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })
		except:
			return Response({ 'code': 400, 'status': 'error', 'data': "An error occurred" })


class CallLogsSave(APIView):
	serializer_class = CallLogSaveSerializer
	def post(self, request):
		data = {}
		# try:
		serializer = self.serializer_class(data = request.data)
		if serializer.is_valid():
			caller_token = serializer.data['caller']
			receiver_phone_number = serializer.data['receiver']
			time = serializer.data['time']
			call_date = serializer.data['call_date']
			screen_id = serializer.data['screen_id']

			if checkToken(caller_token):
				if Profile.objects.filter(phone_number = receiver_phone_number).exists():
					caller_data = Profile.objects.get(token = caller_token)
					CallLog(caller = caller_data.phone_number, receiver = receiver_phone_number, time = time, call_date = call_date, screen_id = screen_id).save()

					data['response_msg'] = 'Log saved successfully'
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'User with the given phone number not found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

			data['response_msg'] = 'Invalid token'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		data['response_msg'] = 'Invalid fields'
		return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })
		# except:
		# 	data['response_msg'] = 'Something went wrong'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })


class CallLogsList(APIView):
	serializer_class = CallLogListSerializer
	def post(self, request):
		data = {}
		# try:
		serializer = self.serializer_class(data = request.data)
		if serializer.is_valid():
			token = serializer.data['token']

			if checkToken(token):
				user_data = Profile.objects.get(token = token)
				user_phone_number = user_data.phone_number
				if CallLog.objects.filter(Q(caller = user_phone_number) | Q(receiver = user_phone_number)).exists():
					logs = CallLog.objects.filter(Q(caller = user_phone_number) | Q(receiver = user_phone_number))

					logs_list = []
					for log in logs:
						caller = log.caller
						receiver = log.receiver
						time = log.time
						call_date = log.call_date
						screen_id = log.screen_id

						if caller == user_phone_number:
							if Contact.objects.filter(user_phone_number = caller).exists():
								get_contacts = Contact.objects.get(user_phone_number = caller)
								contacts_str = get_contacts.contacts
								ph_no = receiver
						else:
							if Contact.objects.filter(user_phone_number = receiver).exists():
								get_contacts = Contact.objects.get(user_phone_number = receiver)
								contacts_str = get_contacts.contacts
								ph_no = caller

						if contacts_str:
							contacts_list = ast.literal_eval(contacts_str)

							for contact in contacts_list:
								temp = {}
								if contact['phone_number'] == ph_no:
									temp['name'] = contact['name']
									temp['phone_number'] = contact['phone_number']

									if contact['profile_image'] == '':
										cont = Profile.objects.get(phone_number = ph_no)
										try:
											temp['profile_image'] = cont.image.url
										except:
											temp['profile_image'] = ''
									else:
										temp['profile_image'] = contact['profile_image']
									
									temp['time'] =  time
									temp['call_date'] =  call_date
									temp['screen_id'] =  screen_id
									logs_list.append(temp)
					
					data['response_msg'] = 'Call logs'
					data['logs'] = logs_list
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				data['response_msg'] = 'no logs found'
				return Response({ 'code': 200, 'status': 'error', 'data': data })

			data['response_msg'] = 'Invalid token'
			return Response({ 'code': 400, 'status': 'error', 'data': data })

		data['response_msg'] = 'Invalid fields'
		return Response({ 'code': 400, 'status': 'error', 'data': data })
		# except:
		# 	data['response_msg'] = 'Something went wrong'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })


class SendNotificationView(APIView):
	serializer_class = SendNotificationSerializer
	def post(self, request):
		data = {}
		serializer = self.serializer_class(data = request.data)
		if serializer.is_valid():
			token = serializer.data['token']
			phone_number = serializer.data['phone_number']
			screen_id = serializer.data['screen_id']

			if checkToken(token):				
				if Profile.objects.filter(phone_number = phone_number).exists():
					receiver_data = Profile.objects.get(phone_number = phone_number)
					receiver_token = receiver_data.token
					sender = Profile.objects.get(token = token)					

					# try:

					if Contact.objects.filter(user_phone_number = receiver_data.phone_number).exists():
						get_contacts = Contact.objects.get(user_phone_number = receiver_data.phone_number)
						contacts_str = get_contacts.contacts						

						# try:

						try:
							profile_image = sender.image.url
						except:
							profile_image = ''

						contacts_list = ast.literal_eval(contacts_str)
						for contact in contacts_list:
							if sender.phone_number == contact['phone_number']:
								if not BlockedContact.objects.filter(blocker = receiver_data.phone_number, blocked_user = contact['phone_number']).exists():

									receiver_device = get_object_or_404(Device, dev_id = receiver_token)
									data_message = { "name" : contact['name'], "phone_number" : contact['phone_number'], "channel_name" : receiver_data.channel_name, 'screen_id': screen_id, "profile_image": profile_image}
									message_title = "Incoming Video call"
									message_body = contact['name'] + ' - ' + contact['phone_number']
									result = push_service.notify_single_device(registration_id = receiver_device.reg_id, message_title = message_title, message_body = message_body, data_message = data_message)									

									data['response_msg'] = result
									return Response({ 'code': 200, 'status': 'success', 'data': data })

					data['response_msg'] = 'No contacts'
					return Response({ 'code': 400, 'status': 'error', 'data': data })
					# except:
					# 	data['response_msg'] = 'Receiver device not found'
					# 	return Response({ 'code': 400, 'status': 'error', 'data': data })

				data['response_msg'] = 'User with the given phone number not found'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
			
			data['response_msg'] = 'Invalid token'
			return Response({ 'code': 400, 'status': 'error', 'data': data })
		
		data['response_msg'] = 'Invalid fields'
		return Response({ 'code': 400, 'status': 'error', 'data': data })
# ------------------------------------------- CHAT, NOTIFICATION STOP -------------------------------------------- #




# ----------------------------------- V2 -----------------------------------
class DeleteDataView(APIView):
	serializer_class = DeleteDataSerializer

	def post(self, request):
		# try:
		serializer = self.serializer_class(data = request.data)

		if serializer.is_valid:
			data = {}
			token = request.POST['token']

			if checkToken(token):
				userdata = Profile.objects.get(token = token)
				phone_number = userdata.phone_number

				# try:
				UserStatus.objects.filter(user = phone_number).delete()
				Contact.objects.filter(user_phone_number = phone_number).delete()
				CallLog.objects.filter(Q(caller = phone_number) | Q(receiver = phone_number)).delete()
				BlockedContact.objects.filter(blocker = phone_number).delete()
				Device.objects.filter(dev_id = token).delete()
				Profile.objects.filter(phone_number = phone_number).delete()
				
				data['response_msg'] = "Account deleted successfully"
				return Response({ 'code': 200, 'status': 'success', 'data': data })
				# except:
				# 	data['response_msg'] = "Error deleting account"
				# 	return Response({ 'code': 400, 'status': 'success', 'data': data })
			else:
				data['response_msg'] = 'Invalid token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })
		else:
			return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })
		# except:
		# 	return Response({ 'code': 400, 'status': 'error', 'data': 'Something went wrong' })


class UserLocationView(APIView):
	serializer_class = UserLocationSerializer

	def post(self, request):
		serializer = self.serializer_class(data = request.data)

		if serializer.is_valid:
			data = {}
			token = request.POST['token']
			lat = request.POST['lat']
			lng = request.POST['lng']

			if checkToken(token):
				userdata = Profile.objects.get(token = token)
				userdata.lat = lat
				userdata.lng = lng
				userdata.save()

				phone_number = userdata.phone_number

				if Contact.objects.filter(user_phone_number = phone_number).exists():
					get_contacts = Contact.objects.get(user_phone_number = phone_number)
					contacts_str = get_contacts.contacts

					locations = []
					contacts_list = ast.literal_eval(contacts_str)
					for contact in contacts_list:
						contact_phone_number = contact['phone_number']

						if Profile.objects.filter(phone_number = contact_phone_number).exists():
							contact_data = Profile.objects.get(phone_number = contact_phone_number)
							
							if (contact_data.lat != '' or contact_data.lng != ''):
								temp = {}
								temp['lat'] = contact_data.lat
								temp['lng'] = contact_data.lng
								temp['name'] = contact['name']
								temp['profile_image'] = contact['profile_image']
								locations.append(temp)

					data['locations'] = locations
					return Response({ 'code': 200, 'status': 'success', 'data': data })

				else:
					data['response_msg'] = 'No contacts found'
					return Response({ 'code': 200, 'status': 'success', 'data': data })
			else:
				data['response_msg'] = 'Invalid token'
				return Response({ 'code': 400, 'status': 'error', 'data': data })

		else:
			return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })


class SendCustomNotificationView(APIView):
	serializer_class = SendCustomNotificationSerializer
	def post(self, request):
		data = {}
		# try:
		serializer = self.serializer_class(data = request.data)
		if serializer.is_valid():
			phone_number = serializer.data['phone_number']
			msg = serializer.data['msg']

			if Profile.objects.filter(phone_number = phone_number).exists():
				# try:
				userdata = Profile.objects.get(phone_number = phone_number)
				token = userdata.token
				user_device = get_object_or_404(Device, dev_id = token)
				message_title = "Notification"
				message_body = msg
				result = push_service.notify_single_device(registration_id = user_device.reg_id, message_title = message_title, message_body = message_body)
				data['response_msg'] = result
				return Response({ 'code': 200, 'status': 'success', 'data': data })
				# except:
				# 	data['response_msg'] = 'Device not found'
				# 	return Response({ 'code': 400, 'status': 'error', 'data': data })				

			data['response_msg'] = 'Invalid phone number'
			return Response({ 'code': 400, 'status': 'error', 'data': data })
		
		data['response_msg'] = 'Invalid fields'
		return Response({ 'code': 400, 'status': 'error', 'data': serializer.errors })

		# except:
		# 	data['response_msg'] = 'Something went wrong'
		# 	return Response({ 'code': 400, 'status': 'error', 'data': data })























