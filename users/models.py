from django.db import models
from fcm.models import AbstractDevice

# Create your models here.
DEVICES_TYPE = (
	('android', 'Android'),
	('ios', 'Ios'),
	('windows', 'Windows')
)

# user info model
class Profile(models.Model):
	name = models.CharField(max_length = 50, blank = True)
	phone_number = models.CharField(max_length = 20)
	# image = models.FileField(upload_to)
	image = models.ImageField(upload_to = 'userprofile/%Y/%m/%d', blank = True)
	otp = models.CharField(max_length = 6, default = '', blank = True)
	token = models.CharField(max_length = 100, default = '', blank = True)
	status = models.TextField(max_length = 30, blank = True)
	device_type = models.CharField(max_length = 10)
	device_id = models.CharField(max_length = 50, blank = True)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)
	is_verified = models.BooleanField(default = 0)
	is_host = models.BooleanField(default = 0)
	channel_name = models.CharField(max_length = 100, blank = True, null = True)
	lat = models.CharField(max_length = 50, blank = True, null = True)
	lng = models.CharField(max_length = 50, blank = True, null = True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'User Profile'
		verbose_name_plural = 'User Profiles'


# contains the contacts of a user in comma separated format
class Contact(models.Model):
	user_phone_number = models.CharField(max_length = 20)
	contacts = models.CharField(max_length = 1000)

	def __str__(self):
		return self.user_phone_number

	class Meta:
		verbose_name = 'Contact'
		verbose_name_plural = 'Contacts'


class BlockedContact(models.Model):
	blocker = models.CharField(max_length = 20)
	blocked_user = models.CharField(max_length = 20)

	def __str__(self):
		return self.blocker

	class Meta:
		verbose_name = 'Blocked Contact'
		verbose_name_plural = 'Blocked Contacts'


class UserStatus(models.Model):
	user = models.CharField(max_length = 20)
	status = models.FileField(upload_to = 'userstatus/%Y/%m/%d', blank = True)
	text_status = models.CharField(max_length = 100, blank = True)
	created_at = models.DateTimeField(auto_now_add = True)
	card_color = models.CharField(max_length = 100, blank = True)
	is_seen = models.BooleanField(default = 0)
	status_type = models.CharField(max_length = 10, help_text = "Wheather uploaded file is image or video.", blank = True)

	def __str__(self):
		return self.user

	class Meta():
		verbose_name = 'User Status'
		verbose_name_plural = 'User Status'


class CallLog(models.Model):
	caller = models.CharField(max_length = 100)
	receiver = models.CharField(max_length = 15)
	time = models.CharField(max_length = 10)
	call_date = models.CharField(max_length = 80)
	screen_id = models.CharField(max_length = 10)

	def __str__(self):
		return self.caller

	class Meta():
		verbose_name = 'Call Log'
		verbose_name_plural = 'Call Logs'