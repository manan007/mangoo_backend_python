from django.urls import path
from users.views import *

urlpatterns = [
	path('api/v1/user/sendotp', SendOTPView.as_view(), name = 'otp_send'),
	path('api/v1/user/verifyotp', VerifyOTPView.as_view(), name = 'otp_verify'),
	path('api/v1/user/addprofile', UserRegisterProfileAdd.as_view(), name = 'add_profile'),

	path('api/v1/user/get_contacts', getWhatsAppContacts.as_view(), name = 'get_contacts'),

	# BLOCKED CONTACT
	path('api/v1/user/blocked_contacts', BlockedContactsView.as_view(), name = 'blocked_contacts'),
	path('api/v1/user/blocked_contacts/add', AddBlockedContactView.as_view(), name = 'add_blocked_contacts'),

	# PROFILE SETTING IN WEBVIEW
	path('api/v1/user/display_profile', DisplayProfileView.as_view(), name = 'display_profile'),
	path('api/v1/user/profile_image/add', UserProfilePhotoChangeView.as_view(), name = 'profile_image_add'),
	path('api/v1/user/name/edit', UserNameChangeView.as_view(), name = 'change_name'),	

	path('privacy_policy', privacy_policy, name = 'privacy_policy'),

	# STATUS
	path('api/v1/user/status/add', AddStatusView.as_view(), name = 'status_add'),
	path('api/v1/user/status/edit', UserStatusChangeView.as_view(), name = 'change_status'),
	path('api/v1/user/status/display', DisplayMyStatusView.as_view(), name = 'status_display_my'),
	path('api/v1/user/status/delete', DeleteStatusView.as_view(), name = 'status_delete'),
	path('api/v1/user/status/contacts/list', ListMyContactStatusView.as_view(), name = 'status_list_my_contacts'),
	path('api/v1/user/status/contacts/display', DisplayContactStatusView.as_view(), name = 'status_display_contacts'),

	path('api/v1/channelname', ChannelNameView.as_view(), name = 'channel_name'),

	path('api/v1/notification/send', SendNotificationView.as_view(), name = 'send_notification'),

	path('api/v1/user/calllogs/add', CallLogsSave.as_view()),
	path('api/v1/user/calllogs/list', CallLogsList.as_view()),


	# ----------------------------------- V2 -----------------------------------
	path('api/v2/user/account/delete', DeleteDataView.as_view()),
	path('api/v2/user/location', UserLocationView.as_view()),
	path('api/v2/user/custom/notification/send', SendCustomNotificationView.as_view()),


]